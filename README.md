Git SHA:  76cb4a1074d10fc3db86a4137bf9f68cc7480e3e 
Project Leader:  Chanakya Remani (Phase 1) 
link to GitLab pipelines: https://gitlab.com/bhaver/relocccate/-/pipelines  
link to website:  www.relocccate.me


| Name | EID | GitLabID |
| ------ | ------ | ------ |
| Riddhi Bhave | rmb3857 | @bhaver |
| Saomya Tangri | st33578 | @saomya |
| Blake Bottum | bb38845 | @blakebottum |
| Chanakya Remani | cr42748 | @chanakya.remani |
| Pamela Lim | pml728 | @pamelamkpk2015 |

**Phase 1 Completion Time** 

| Name | Estimated | Actual |
| ------ | ------ | ------ |
| Riddhi Bhave | 10 hrs | 15 hrs |
| Saomya Tangri | 12 hrs | 12 hrs |
| Blake Bottum | 11 hrs | 12 hrs |
| Chanakya Remani | 15 hrs | 12 hrs |
| Pamela Lim | 11 hrs | 15 hrs |


**Phase 2 Completion Time** 

| Name | Estimated | Actual |
| ------ | ------ | ------ |
| Riddhi Bhave | 30 hrs | |
| Saomya Tangri | | |
| Blake Bottum | | |
| Chanakya Remani | | |
| Pamela Lim | | |

In the project directory, you can run:

**Installation and Requirements**

This project requires having node and npm installed, so you can check if you have the required dependencies using `which npm` or `which node` on macOS and `where npm` or `where node` for Linux/Windows. Make sure you have the most up to date versions installed by using `npm update`. Then, use `npm install` in order to install any missing dependencies.

In order to run this project, run `npm start` inside the frontend/backend folder respectively. Alternatively, you can run `npm run build` which runs the application in the build folder.

To deploy the entire project to a production server, run `make docker-deploy` in the root directory of the project.

Comments:  
We had fun! 
