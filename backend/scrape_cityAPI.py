from flask import Flask, request, jsonify, redirect, json, Response
import requests
from setup import db
from models.college import College
from models.city import City
from models.county import County
from ratelimit import limits, sleep_and_retry

def scrape_city():
  
    #ONLY FOR TESTING DELETED BELOW WHEN ALL MODELS ARE DONE -------------------------

    db.drop_all()
    db.create_all()
    db.session.commit()
    county1 = County(id=2, name="county1", lat=2, long=3, temp=4,pressure=5,humidity=6,wind=7,uvi=8,photo_ref="photo")
    county3 = County(id=3, name="county3", lat=2, long=3, temp=4,pressure=5,humidity=6,wind=7,uvi=8,photo_ref="photo")
    db.session.add(county1)
    db.session.add(county3)
    db.session.commit()
    # city1 = City(id=600, county_id=county1.id,name="San Francisco", lat=3,long=4,lgbtq=5,medical=6,overall=7,physical=8, political=9, theft=10,women=11,photo_ref="photo")
    # city2 = City(id=601, county_id=county1.id,name="Austin", lat=3,long=4,lgbtq=5,medical=6,overall=7,physical=8, political=9, theft=10,women=11,photo_ref="photo")
    college1 = College(id=1,county="county3",name="college1",city="San Francisco",state= "California",url="url1",zip="zip1",lat=37.810980,long=-122.483716,total_enroll=1000,
        undergrad_enroll=5000,
        avg_sat=3500,
        admin_rate=10,
        photo_ref="photo1")
    college2 = College(id=2,county="county3",name="college2",city="Dallas",state= "Texas",url="url2",zip="zip2",lat=32.806993,long=-96.836857,total_enroll=1000,
        undergrad_enroll=5000,
        avg_sat=3500,
        admin_rate=10,
        photo_ref="photo2")

    db.session.add(college1)
    db.session.add(college2)    
    # db.session.add(city1)
    # db.session.add(city2)
    db.session.commit()
    counties = County.query.all()
    colleges = College.query.all()


    #ONLY FOR TESTING. DELETE ABOVE WHEN ALL MODELS ARE DONE ----------------

    count = 1
    #initilzed to first access token granted for safety data
    access_token = "i4qksKUDW9GsNck6VCiG3QKjffBB"
    for college in colleges:
        college_city = college.city
        college_state = college.state
        college_lat = college.lat
        college_long = college.long
        #make sure county field is added in college model
        college_county = college.county
        # check if this city,state already exist in city model
        query = db.session.query(City).filter_by(name=college_city, state=college_state).first()
        unique = True
        if (query is not None):
            this_lat = query.lat
            this_long = query.long
            # difference of 1 in latitide is about 70 miles and in longtitude about 54 miles
            if abs(this_lat - college_lat) < 1 and abs(this_long - college_long) < 1:
                unique = false

        if unique:
            # get county_id:
            # find college_county in county model and get corresponding id 
            city_county_id = db.session.query(County).filter_by(name=college_county).first().id

            # #get safety data 
            while True:
                endpoint = "https://test.api.amadeus.com/v1/safety/safety-rated-locations?latitude=" + str(college_lat) + "&longitude=" + str(college_long) + "&radius=20"
                headers = {"Authorization": "Bearer " + access_token}
                safety_data = requests.get(endpoint, headers=headers)
                if(safety_data.status_code == 200):
                    break
                if(safety_data.status_code == 401):
                    # access token for safety data expired so request another token and try calling the safety api again
                    endpoint = "https://test.api.amadeus.com/v1/security/oauth2/token"
                    data = {"grant_type" : "client_credentials", "client_id" : "iStu41awlqKdAerPEZ8RjiyQMbgNfd5X", "client_secret": "YhofyO94BzpC8FY4"}

                    access_token = requests.post(endpoint, data=data).json()["access_token"]


            # get photo ref data
            city_photo_ref = getPhotoRef(college_city, college_state)

            # build city object
            city = None
            try:
                scores = safety_data.json()["data"][0]["safetyScores"]
                print(scores)
                # city = City(id=count, county_id=city_county_id, name=college_city, state=college_state, 
                #     lat=college_lat, long=college_long,lgbtq=score["lgbtq"],medical=scores["medical"],
                #     overall=["overall"],physical=["physicalHarm"],political=["politicalFreedom"],theft=["theft"],
                #     women=["women"],photo_ref=city_photo_ref)

                #UNCOMMENT ABOVE WHEN COUNTY DATA IS AVAILABLE
                city = City(id=count, name=college_city, state=college_state, 
                    lat=college_lat, long=college_long,lgbtq=scores["lgbtq"],medical=scores["medical"],
                    overall=scores["overall"],physical=scores["physicalHarm"],political=scores["politicalFreedom"],theft=scores["theft"],
                    women=scores["women"],photo_ref=city_photo_ref)

            # no safety data available - all values are 0 for integers and N/A for strings 
            except:
                # city = City(id=count, county_id=1, name="N/A", state="N/A", 
                #     lat=0, long=0,lgbtq=0,medical=0, overall=0,physical=0,political=0,theft=0,
                #     women=0,photo_ref="N/A")

                #UNCOMMENT ABOVE WHEN COUNTY DATA IS AVAILABLE
                city = City(id=count, name="N/A", state="N/A", 
                    lat=0, long=0,lgbtq=0,medical=0, overall=0,physical=0,political=0,theft=0,
                    women=0,photo_ref="N/A")

            db.session.add(city)
            db.session.commit()     
            count += 1

    print(City.query.all())
    #GET RID OF RETURN RESPONSE WHEN DONE WITH TESTING
    return Response("test", status=200)



@sleep_and_retry
@limits(calls=50, period=1)
def getPhotoRef(college_city, college_state):
    # get photo ref data
    try:
        search_endpoint = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=" + college_city + "%20" + college_state + "&inputtype=textquery&fields=photos&key=AIzaSyBr_UyDMGu9CCGCcDO1OMAkVfQOoBAyegU"
        print(search_endpoint)
        search_response = requests.get(search_endpoint)
        photo_ref = search_response.json()["candidates"][0]["photos"][0]["photo_reference"]
    except:
        photo_ref = "N/A"

    return photo_ref