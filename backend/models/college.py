from flask_sqlalchemy import SQLAlchemy
from setup import db
from dataclasses import dataclass

#create College Model
@dataclass
class College(db.Model):
    __tablename__ = "college"

    id: int = db.Column(db.Integer, primary_key=True, autoincrement=False)

    city_id = db.Column(db.Integer, db.ForeignKey("city.id"))
    county_id = db.Column(db.Integer, db.ForeignKey("county.id"))
    county: str = db.Column(db.String(64))

    name: str = db.Column(db.String(64))
    city: str = db.Column(db.String(64))
    county: str = db.Column(db.String(64))
    state: str = db.Column(db.String(8))
    url: str = db.Column(db.String(64))
    zip: str = db.Column(db.String(16))
    lat: float = db.Column(db.Float)
    long: float = db.Column(db.Float)
    total_enroll: int = db.Column(db.Integer)
    undergrad_enroll: int = db.Column(db.Integer)
    graduate_enroll: int = db.Column(db.Integer)
    avg_sat: int = db.Column(db.Integer)
    admin_rate: float = db.Column(db.Float)
    photo_ref: str = db.Column(db.String(256))

    def connected(self):
        return {
            "city_info": self.city_info,
            "county_info": self.county_info
        }
