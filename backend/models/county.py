from flask_sqlalchemy import SQLAlchemy
from setup import db
from dataclasses import dataclass

#Create County Model
@dataclass
class County(db.Model):
    __tablename__ = "county"
    
    id: int = db.Column(db.Integer, primary_key=True, autoincrement=False)

    city_list = db.relationship("City", backref="county_info")
    college_list = db.relationship("College", backref="county_info")

    name: str = db.Column(db.String(64))
    lat: float = db.Column(db.Float)
    long: float = db.Column(db.Float)
    temp: float = db.Column(db.Float)
    pressure: float = db.Column(db.Float)
    humidity: float = db.Column(db.Float)
    wind: float = db.Column(db.Float)
    uvi: float = db.Column(db.Float)
    photo_ref: str = db.Column(db.String(256))

    def connected(self):
        return {
            "college_list": self.college_list,
            "city_list": self.city_list
        }
