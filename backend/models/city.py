from flask_sqlalchemy import SQLAlchemy
from setup import db
from dataclasses import dataclass

#Create City Model
@dataclass
class City(db.Model):
    __tablename__ = "city"

    id: int = db.Column(db.Integer, primary_key=True, autoincrement=False)

    college_list = db.relationship("College", backref="city_info")
    county_id = db.Column(db.Integer, db.ForeignKey("county.id"))

    name: str = db.Column(db.String(64))
    state: str = db.Column(db.String(64))
    lat: float = db.Column(db.Float)
    long: float = db.Column(db.Float)
    lgbtq: int = db.Column(db.Integer)
    medical: int = db.Column(db.Integer)
    overall: int = db.Column(db.Integer)
    physical: int = db.Column(db.Integer)
    political: int = db.Column(db.Integer)
    theft: int = db.Column(db.Integer)
    women: int = db.Column(db.Integer)
    photo_ref: str = db.Column(db.String(256))

    def connected(self):
        return {
            "college_list": self.college_list,
            "county_info": self.county_info
        }
