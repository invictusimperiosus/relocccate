from flask import Flask, json
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

sql_info = json.load(open("./private.json"))

#initialize app
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://%s:%s@%s/%s" % (sql_info["username"], sql_info["password"], sql_info["endpoint"], sql_info["db_name"])
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["JSON_SORT_KEYS"] = False
app.config["TRAP_HTTP_EXCEPTIONS"] = True
app.url_map.strict_slashes = False

#intialize database
db = SQLAlchemy(app)