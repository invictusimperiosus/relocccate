from flask import Flask, request, jsonify, redirect, json
from setup import app, db
from werkzeug.exceptions import *
from schema import filter_functions, conform_bulk
from models.college import College
from models.city import City
from models.county import County
from dataclasses import dataclass, asdict
from flask_sqlalchemy import SQLAlchemy
import requests
from scrape_cityAPI import getPhotoRef


def scrapeCollege():
    db = SQLAlchemy(app)

    # FOR DEBUGGING ONLY. DELETE LATER

    # for i in range(501):
    #     deleted_objects = College.__table__.delete().where(College.id.in_([i]))
    #     db.session.execute(deleted_objects)
    # db.session.commit()

    # College.query.filter(College.id == i).delete()
    # db.session.commit()

    # college_elem = College(
    #                         id = 500,
    #                         county = "Travis", #dummy, change later"
    #                         name = "UT",
    #                         city = "Austin"#,
    #                     #     state = college["school.state"],
    #                     #     url = college["school.school_url"],
    #                     #     zip = college["school.zip"],
    #                     #     lat = college["location.lat"],
    #                     #     long = college["location.lon"],
    #                     #     total_enroll = college["latest.student.enrollment.undergrad_12_month"] + grad_num,
    #                     #     undergrad_enroll = college["latest.student.enrollment.undergrad_12_month"],
    #                     #     graduate_enroll = college["latest.student.enrollment.grad_12_month"],
    #                     #     avg_sat = college["latest.admissions.sat_scores.average.overall"],
    #                     #     admin_rate = college["latest.admissions.admission_rate.overall"],
    #                     #     photo_ref = getPhotoRef(college["school.city"], college["school.state"])
    #                         )
    # db.session.add(college_elem)        
    # db.session.commit()


    id_cnt = 0
    for page in range(5):
        college_json = requests.get(
            "https://api.data.gov/ed/collegescorecard/v1/schools.json?" +
            "latest.admissions.sat_scores.average.overall__range=1170.." +
            "&fields=latest.student.size,latest.student.enrollment.undergrad_12_month," +
            "latest.student.enrollment.grad_12_month,school.school_url,school.zip,school.name," +
            "school.city,school.state,location.lat,location.lon,latest.admissions.sat_scores," +
            "latest.admissions.admission_rate&per_page=100&page=" +
            str(page) +
            "&api_key=C0NVkh5vYaqYDHWsuPGTso8av3tIpYO3hZNoxhse"
        ).json()

        for college in college_json["results"]:
            grad_num = 0
            if college["latest.student.enrollment.grad_12_month"] is not None:
                grad_num = college["latest.student.enrollment.grad_12_month"]

            college_elem = College(
                            id = id_cnt,
                            # city_id = id_cnt,
                            # county_id = id_cnt,
                            county = "Travis", # dummy, change later"
                            name = college["school.name"],
                            city = college["school.city"],
                            state = college["school.state"],
                            url = college["school.school_url"],
                            zip = college["school.zip"],
                            lat = college["location.lat"],
                            long = college["location.lon"],
                            total_enroll = college["latest.student.enrollment.undergrad_12_month"] + grad_num,
                            undergrad_enroll = college["latest.student.enrollment.undergrad_12_month"],
                            graduate_enroll = college["latest.student.enrollment.grad_12_month"],
                            avg_sat = college["latest.admissions.sat_scores.average.overall"],
                            admin_rate = college["latest.admissions.admission_rate.overall"],
                            photo_ref = getPhotoRef(college["school.city"], college["school.state"])
                        )
            db.session.add(college_elem)
            id_cnt += 1
    db.session.commit()
