from werkzeug.exceptions import *
from typing import Callable
from setup import db
from dataclasses import fields

# Returns to_set if value is not of type the_type
def ensure_type(value, the_type: type, to_set):
    if not isinstance(value, the_type):
        return to_set
    return value

# Ensures that params conforms to a given dictionary schema
def conform_params(params, conform: dict, model: db.Model):
    if not isinstance(params, dict):
        raise BadRequest("Invalid Parameters")
    returned = {}
    # for each parameter in conform, ensure conformation
    for param in conform:
        info = conform[param]
        exists = param in params
        # if this parameter does not need to exist, continue
        if not exists and not info.get("use_nonexistant", True):
            continue
        # check if parameter is value, if not use on_invalid to get a valid instance
        value = params.get(param)
        if info["valid"](value, model):
            returned[param] = value
        else:
            on_invalid = info["on_invalid"]
            # if on_invalid is a function, call it with value, else assign its raw value
            if isinstance(on_invalid, Callable):
                returned[param] = on_invalid(value, exists)
            else:
                returned[param] = on_invalid
        
    return returned

def raise_helper(value, do_raise):
    if do_raise:
        raise BadRequest("Invalid Parameters")
    return value

def raise_or_value(value):
    return lambda _, exists : raise_helper(value, exists)

# root schema dictionary
conform_root = {
    "limit": {
        "valid": lambda limit, _ : limit == None or (type(limit) is int and limit >= 0),
        "on_invalid": raise_or_value(None)
    },
    "offset": {
        "valid": lambda offset, _ : type(offset) is int and offset >= 0,
        "on_invalid": raise_or_value(0)
    },
    "sort": {
        "valid": lambda sort, _ : type(sort) is dict and len(sort) > 0,
        "on_invalid": raise_or_value({
            "id": "asc"
        })
    },
    "filter": {
        "valid": lambda filter, _ : type(filter) is dict,
        "on_invalid": raise_or_value({})
    }
}

# all valid filter parameters
filter_functions = {
    "contains": {
        "types": (str, ),
        "allow_null": True,
        "func": lambda a,b : a.like("%%%s%%" % b),
        "combine": "or_"
    },
    "==": {
        "types": (str, int, float),
        "allow_null": True,
        "func": lambda a,b : a == b,
        "combine": "or_"
    },
    "!=": {
        "types": (str, int, float),
        "allow_null": True,
        "func": lambda a,b : db.case(
            [
                (a.isnot(None), a != b)
            ],
            else_ = (db.literal(b) != None)
        ),
        "combine": "and_"
    },
    "<": {
        "types": (str, int, float),
        "allow_null": False,
        "func": lambda a,b : a < b,
        "combine": "or_"
    },
    "<=": {
        "types": (str, int, float),
        "allow_null": False,
        "func": lambda a,b : a <= b,
        "combine": "or_"
    },
    ">": {
        "types": (str, int, float),
        "allow_null": False,
        "func": lambda a,b : a > b,
        "combine": "or_"
    },
    ">=": {
        "types": (str, int, float),
        "allow_null": False,
        "func": lambda a,b : a >= b,
        "combine": "or_"
    }
}

def convert_type(the_type):
    if the_type in (int, float):
        return (int, float)
    return (the_type, )

def valid_func(value, model, key, func):
    func_data = filter_functions[func]
    
    converted = model.__annotations__[key]
    if not converted in func_data["types"]:
        print(converted)
        raise BadRequest("Function '%s' cannot be run on column '%s'" % (func, key))

    converted = convert_type(converted)

    null_check = (lambda value : value == None) if func_data["allow_null"] else (lambda value : False)

    if type(value) is list:
        return all(null_check(elem) or isinstance(elem, converted) for elem in value)
    elif null_check(value) or isinstance(value, converted):
        return False
    else:
        raise BadRequest("Function '%s' in column '%s' cannot be run with value '%s'" % (func, key, str(value)))

def valid_filter(key, func):
    return lambda value, model : valid_func(value, model, key, func)

# filter schema dictionary
def conform_funcs(key):
    conform_funcs = {
        "options": {
            "valid": lambda value, _ : type(value) is dict,
            "on_invalid": raise_or_value({})
        }
    }
    for func in filter_functions:
        conform_funcs[func] = {
            "valid": valid_filter(key, func),
            "on_invalid": lambda value, _ : raise_helper([value], type(value) is list),
            "use_nonexistant": False
        }
    return conform_funcs

# options schema dictionary
conform_options = {
    "ignore_case": {
        "valid": lambda ignore, _ : type(ignore) is bool,
        "on_invalid": raise_or_value(True)
    },
    "where_params": {
        "valid": lambda value, _ : value in ("intersect", "union"),
        "on_invalid": raise_or_value("intersect")
    }
}

def valid_attr(attr, model):
    return isinstance(attr, str) and attr in (field.name for field in fields(model))

def conform_bulk(request, model):
    # ensure parameters has the correct schema
    parameters = {} if len(request.data) == 0 else request.get_json(force=True)
    parameters = conform_params(
        parameters,
        conform_root,
        model
    )
    sort = parameters["sort"]
    # ensure sort has the correct schema
    for key in sort:
        if not valid_attr(key, model):
            raise BadRequest("%s is not a valid column" % str(key))
        elif not sort[key] in ("asc","desc"):
            raise BadRequest("%s is not a valid sorting order" % str(sort[key]))
    # ensure filter has the correct schema
    filter = parameters["filter"]
    for key in filter:
        if not valid_attr(key, model):
            raise BadRequest("%s is not a valid column" % str(key))
        # if it is a raw value, make it a dictionary with parameter equals
        filter[key] = ensure_type(filter[key], dict, {
            "==": filter[key]
        })
        # ensure the conformation of the filter parameters
        filter[key] = conform_params(
            filter[key],
            conform_funcs(key),
            model
        )
        filter[key]["options"] = conform_params(
            filter[key]["options"],
            conform_options,
            model
        )
    
    return parameters