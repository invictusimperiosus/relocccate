from flask import Flask, request, jsonify, redirect, json
from setup import app, db
from werkzeug.exceptions import *
from schema import filter_functions, conform_bulk
from models.college import College
from models.city import City
from models.county import County
from dataclasses import dataclass, asdict
from flask_sqlalchemy import SQLAlchemy
import requests
from scrape_cityAPI import getPhotoRef
from scrape_collegeAPI import scrapeCollege

#TODO (populating database):
    # 1.test out database with dummy data:
        #make sample city object with all table params
        #db.session.add(city_object)
        #db.session.commit()    ---- commits change to the database
    # 2. scrape all external API (weather, colleges, safety):
        # loop through response json and make object (city, college, or county) object 
        # populate database with created objects (add, commit)

# scrapeCollege() #delete later

@app.before_request
def clear_trailing():
    rp = request.path 
    if rp != "/" and rp.endswith("/"):
        return redirect(rp[:-1])

model_table = {
    "colleges": College,
    "cities": City,
    "counties": County
}

@app.route("/<string:model>", methods=["POST","GET"])
def bulk(model: str):

    model = model_table.get(model)

    if model == None:
        raise NotFound()
    
    parameters = conform_bulk(request, model)

    # we now have a properly formatted parameter dictionary, we can use this to fetch from our database

    query = model.query

    filter_list = []

    # for each filtered column
    for to_filter in parameters["filter"]:
        filter_info = parameters["filter"][to_filter]
        options = filter_info["options"]
        to_append = []
        # run each filter function
        for to_run in filter_info:
            operate = filter_functions.get(to_run)
            if operate == None:
                continue
            # TODO: implement ignore_case option
            column = getattr(model, to_filter)
            func = operate["func"]
            to_append.append(
                getattr(db, operate["combine"])(
                    func(column, value) for value in filter_info[to_run]
                )
            )
        # handle where_params option
        if options["where_params"] == "intersect":
            to_append = db.and_(x for x in to_append)
        else:
            to_append = db.or_(x for x in to_append)
        filter_list.append(to_append)

    # run the filter query
    query = query.filter(db.and_(x for x in filter_list))
    # sort by options
    query = query.order_by(*[
        getattr(
            getattr(model, key),
            parameters["sort"][key]
        )()
        for key in parameters["sort"]
    ])
    # get limit and offset
    if parameters["limit"] != None:
        query = query.limit(parameters["limit"])
    query = query.offset(parameters["offset"])
    query = query.all()

    # return result
    return jsonify({
        "status": 200,
        "metadata": {
            "found": len(query)
        },
        "data": query
    })

@app.route("/<string:model>/<int:id>", methods=["GET"])
def info(model: str, id: int):

    model = model_table.get(model)

    if model == None:
        raise NotFound()

    # get item with id
    result = model.query.get(id)

    found = result != None

    # if we have data
    if found:
        # get connected models
        connected = result.connected()

        # turn result into dict
        result = asdict(result)

        # add all models into result
        for connection in connected:
            result[connection] = connected[connection]

    # return result
    return jsonify({
        "status": 200 if found else 404,
        "metadata": {
            "found": 1 if found else 0
        },
        "data": result
    })

def handle_exception(e):

    http_error = isinstance(e, HTTPException)

    return jsonify({
        "status": e.code if http_error else 500,
        "error": e.name if http_error else "Internal Server Error",
        "desc": str(e)
    })

app.register_error_handler(Exception, handle_exception)

if __name__ == "__main__":
    app.run()