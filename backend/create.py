from models.college import College
from models.city import City
from models.county import County
from setup import app, db

db.create_all()