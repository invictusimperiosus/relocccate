# Source: Collatz Project makefile
.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

ifeq ($(shell uname -s), Darwin)
    BLACK         := black
    CHECKTESTDATA := checktestdata
    COVERAGE      := coverage3
    MYPY          := mypy
    PYDOC         := pydoc3
    PYLINT        := pylint
    PYTHON        := python3
else ifeq ($(shell uname -p), unknown)
    BLACK         := black
    CHECKTESTDATA := checktestdata
    COVERAGE      := coverage
    MYPY          := mypy
    PYDOC         := pydoc
    PYLINT        := pylint
    PYTHON        := python
else
    BLACK         := black
    CHECKTESTDATA := checktestdata
    COVERAGE      := coverage3
    MYPY          := mypy
    PYDOC         := pydoc3
    PYLINT        := pylint3
    PYTHON        := python3
endif

# TODO: Implement commands

docker-kill:

# Run the frontend using frontend/Dockerfile
docker-front:

# Run the frontend using backend/Dockerfile
docker-back:

# Run all servers
docker-deploy:
	docker-compose down
	docker container prune -f
	docker-compose build
	docker image prune -f
	docker-compose up

# Remove all temporary files
clean:

# Format all code files
format: