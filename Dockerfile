FROM nginx:alpine

ENTRYPOINT ["nginx"]
CMD ["-g", "daemon off;"]