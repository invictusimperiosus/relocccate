import React from "react";


export default function InfoCards(props:any){
    return (
        <div
        className="card"
        style={{ width: "15rem", margin: "0.25rem 0.25rem 0.25rem 0.25rem" }}
      >
        <img className="card-img-top" src={props.info.image} alt={props.info.name[0]}></img>
        <div className="card-body">
          <h5 className="card-title">{props.info.name[0]}</h5>
          <p className="card-text">{props.info.desc}</p>
        </div>
        <ul className="list-group list-group-flush">
          <li className="list-group-item ">
            <p className="card-text">
              <strong>Role: </strong>
              {props.info.jobs}
            </p>
          </li>
          <li className="list-group-item ">
            <strong>Gitlab:</strong> {props.info.gitlab}
            <ul>
              <li>
                {"Commits: "} {props.info.commits}
              </li>
              <li>
                {"Issues: "}
                {props.info.issues}
              </li>
              <li>
                {"Unit Tests: "}
                {props.info.tests}
              </li>
            </ul>
          </li>
        </ul>
      </div>
      );
    

        
}