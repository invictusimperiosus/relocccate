import React from "react";
import { NavLink, Link } from "react-router-dom";
import "./Navbar.css";
import logo from "../images/relocccatelogo.png";

// Creates a bar with links to the model pages, about page, and home page.
// This is used on the top of our site.
function CustomNavbar() {
  const elements = [
    // { link: "/", text: "\nHome" },
    { link: "/about", text: "\nAbout" },
    { link: "/colleges", text: "\nColleges" },
    { link: "/cities", text: "\nCities" },
    { link: "/counties", text: "\nCounties" },
    
  ];

  function make() {
    return elements.map((elem)=>(
      <li key={elem.text} className={"nav-item"}>
        <NavLink
          exact to={elem.link}
          className={"nav-link"}
          activeClassName="active"
        >
          {elem.text}
        </NavLink>
      </li>
    ));
  }

  return (
    <nav className={"navbar navbar-expand-lg navbar-custom sticky-top py-0"}>
      <Link to="/" className={"navbar-brand"}>
        Relo
        <img className="icon" src={logo} alt="logo" />
        ate
      </Link>
      <button
        className={"navbar-toggler"}
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className={"navbar-toggler-icon"}></span>
      </button>

      <div className={"collapse navbar-collapse"} id="navbarSupportedContent">
        <ul className={"navbar-nav mr-auto"}>{make()}</ul>
      </div>
    </nav>

  );
}

export default CustomNavbar;
