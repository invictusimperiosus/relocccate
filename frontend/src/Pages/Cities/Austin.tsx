import React from "react";
import "./Austin.css";

import { LatLngExpression } from "leaflet";
import "leaflet/dist/leaflet.css";
import { Link } from 'react-router-dom';
import Card from 'react-bootstrap/Card'
import CardGroup from 'react-bootstrap/CardGroup'
import { MapContainer, Popup, TileLayer, CircleMarker } from "react-leaflet";

import UT from '../../images/UT.jpg'
import weather from '../../images/AustinWeather.jpg'

function Austin(){
    /* Define constants to be used for map component */
    /* NOTE: will refactor into interface for phase 2 to avoid repetition*/
    const position : LatLngExpression = [30.2672, -97.7431];
    const zoom = 15

    const color = { color: 'red', fillColor: 'red' }
    return (
        <div>
            <h2>Austin, Texas</h2>
            {/* Use MapContainer to create interactive map that allows the user to scroll and view geolocation */}
            <MapContainer center={position} zoom={zoom} scrollWheelZoom={false} style={{height : '300px'}}>
                <TileLayer
                    attribution="&copy; <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors"
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {/* create circle pinpoint at given longitude and latitude to identify core region */}
                <CircleMarker center={position} pathOptions={color} radius={40}>
                    <Popup>Austin City Center</Popup>
                </CircleMarker>
            </MapContainer>
            {/* use CardGroup to display text in neat card format */}
            <CardGroup>
                <Card>
                    {/* display statistics in first card */}
                    <Card.Title> <strong> Safety Scores </strong> </Card.Title>
                    <Card.Body> 
                        <Card.Text> LGBTQ+: 0 </Card.Text>
                        <Card.Text> Medical: 0 </Card.Text>
                        <Card.Text> Overall: 0 </Card.Text>
                        <Card.Text> Physical Harm: 0 </Card.Text>
                        <Card.Text> Political Freedom: 0 </Card.Text>
                        <Card.Text> Theft: 0 </Card.Text>
                        <Card.Text> Women: 0 </Card.Text>
                    </Card.Body>
                </Card>
                <Card>
                    {/* display image and link to college in second card */}
                    <Card.Img variant="top" src={UT} />
                    <Card.Body>
                        <Card.Title>Explore Nearby Universities</Card.Title>
                        <Card.Text>
                            <Link to="/Uta">UT Austin</Link>
                        </Card.Text>
                    </Card.Body>
                </Card>
                <Card>
                    {/* display image and link to county in third card */}
                    <Card.Img variant="top" src={weather} />
                    <Card.Body>
                        <Card.Title>Explore County Weather</Card.Title>
                        <Card.Text>
                            <Link to="/Travis">Travis County</Link>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </CardGroup>
        </div>
    )
}

export default Austin;