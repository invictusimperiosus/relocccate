import * as React from "react";
import { Link } from 'react-router-dom';

import "./Cities.css";

import Table from 'react-bootstrap/Table'

function Cities(){
    return (
        <div style={{margin: 'auto', marginTop: '15px', width: '80%'}}> 
            <h2>Cities</h2>
            {/* Create a table view to display all the elements for the cities */}
            <Table striped bordered hover>

                <thead>
                    <tr>
                        <th rowSpan={2}>City</th>
                        <th rowSpan={2}>State</th>
                        <th rowSpan={2}>Latitude</th>
                        <th rowSpan={2}>Longitude</th>
                        {/* Use column span of 7 for safety scores to create overarching header for headers */}
                        <th colSpan={7}>Safety Scores</th>
                    </tr>
                    <tr>
                        {/* Create subheaders for safety score header */}
                        <th>LGBTQ+</th>
                        <th>Medical</th>
                        <th>Overall</th>
                        <th>Physical Harm</th>
                        <th>Political Freedom</th>
                        <th>Theft</th>
                        <th>Women</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><Link to="/NYC">New York City</Link></td>
                        <td>New York</td>
                        <td>40.71</td>
                        <td>-74.00</td>
                        <td>35</td>
                        <td>73</td>
                        <td>39</td>
                        <td>30</td>
                        <td>40</td>
                        <td>27</td>
                        <td>26</td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td><Link to="/Austin">Austin</Link></td>
                        <td>Texas</td>
                        <td>30.26</td>
                        <td>-97.74</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td><Link to="/Dallas">Richardson</Link></td>
                        <td>Texas</td>
                        <td>32.78</td>
                        <td>-96.80</td>
                        <td>51</td>
                        <td>70</td>
                        <td>50</td>
                        <td>50</td>
                        <td>42</td>
                        <td>50</td>
                        <td>35</td>
                    </tr>
                </tbody>
            </Table>
            <p>* Note: Data fields which contain zeroes means that data is not available.</p>
        </div>
    )
}

export default Cities;