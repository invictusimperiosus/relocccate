import riddhi from "./AboutPhotos/riddhi.jpg";
import blake from "./AboutPhotos/blake.jpg";
import pamela from "./AboutPhotos/Pamela.jpg";
import saomya from "./AboutPhotos/Saomya.jpg";
import chana from "./AboutPhotos/chanakya.jpg";

export const metaData = {
    numCommits: 0,
    numIssues: 0,
    numTests: 0
}

export const memberInfo = [
    {
        name: ["Riddhi Bhave"],
        desc: "Hi! I'm a third year computer science major from Pennsylvania! I like to read and play golf in my free time.",
        jobs: "Fullstack Developer",
        image: riddhi,
        gitlab: "bhaver",
        commits: 0,
        issues: 0,
        tests: 0,
        email: "bhave.riddhi@gmail.com"
    },
    {
        name: ["Blake Bottum"],
        desc: "Hi, my name is Blake and I am a sophomore CS major at UT. I enjoy playing video games and tennis.",
        jobs: "Fullstack Developer",
        image: blake,
        gitlab: "blakebottum",
        commits: 0,
        issues: 0,
        tests: 0,
        email: "blakeplatypus@gmail.com"
    },
    {
        name: ["Saomya Tangri", "Saomya"],
        desc: "Hi, my name is Saomya and I’m a junior CS major at UT. Some of my hobbies include dancing, playing guitar, and running. ",
        jobs: "Fullstack Developer",
        image: saomya,
        gitlab: "saomya",
        commits: 0,
        issues: 0,
        tests: 0,
        email: "saomyatangri@gmail.com"
    },
    {
        name: ["Pamela Lim","pamelamkpk"],
        desc: "Hi, my name is Pamela and I am a sophomore. I enjoy reading, playing board games, and crocheting.",
        jobs: "Fullstack Developer",
        image: pamela,
        gitlab: "pamelamkpk2015",
        commits: 0,
        issues: 0,
        tests: 0,
        email: "pamelamkpk2015@gmail.com"
    },
    {
        name: ["Chanakya Remani","Chanakya"],
        desc: "Hi, my name is Chanakya and I’m a junior at UT. I like watching sports and travelling the world.",
        jobs: "Phase 1 Leader, Fullstack Developer",
        image: chana,
        gitlab: "chanakya.remani",
        commits: 0,
        issues: 0,
        tests: 0,
        email: "chanakya.remani@gmail.com"
    }
];