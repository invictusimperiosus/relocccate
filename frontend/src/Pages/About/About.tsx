import AboutTop from "../../Components/AboutTop";
import "./About.css";
import axios from "axios";
import {memberInfo,metaData} from "./MemberInfo";
import React, {useEffect, useState} from "react";

type dataCallback = (response: any) => Promise<void>;

//Helper function for sequentially fetching multiple pages of info
async function makeRequests(url: string, onData: dataCallback, perPage: number = 400){
    let page = 1;
    while(true){
        //Make the request
        let response = await axios.get(`${url}?per_page=${perPage}&page=${page++}`);
        let data = response.data;
        //If there is no data, we are done
        if(data == null || data.length === 0){
            return;
        }
        //Send the request to the user defined function
        await onData(data);
    }
}

export default function About(){
    //memberInfo is array of information about all the team members
    const [members, setMembers] = useState(memberInfo);
    const [meta, setMeta] = useState(metaData);

    //Only fetch data on initial render
    useEffect(() => {
        
        members.forEach(member => {
            member.commits = 0;
            member.issues = 0;
            member.tests = 0;
        });
        meta.numCommits = 0;
        meta.numIssues = 0;
        meta.numTests = 0;

        //Execute fetches in parallel
        Promise.all([
            makeRequests("https://gitlab.com/api/v4/projects/24626603/repository/commits", async data => {
                //Find the commits for each member
                members.forEach(member => {
                    //takes into account all possible names associated with a member
                    let memberCommits = data.filter((commit: any) => member.name.includes(commit.committer_name)).length;
                    member.commits += memberCommits;
                });
                meta.numCommits += data.length;
            }),
            makeRequests("https://gitlab.com/api/v4/projects/24626603/issues", async data => {
                //Find the issues for each member
                members.forEach(member => {
                    //takes into account all possible names associated with a member
                    let memberIssues = data.filter((issue: any) => member.name.includes(issue.author.name)).length;
                    member.issues += memberIssues;
                });
                meta.numIssues += data.length;
            })
        ]).then(() => {
            //Update components
            setMeta({
                numCommits: meta.numCommits,
                numTests: meta.numTests,
                numIssues: meta.numIssues
            });
            setMembers(members);
        });

    });
    
    return (
        <div className="about-body pb-4">
            <div className="container-fluid pt-2">
                <AboutTop/>
                <div className="container pt-4">
                    <h2 className={"h2-title"}>GitLab Information</h2>
                    <ul>
                        <li>
                            <strong>Commits</strong>: {meta.numCommits}
                        </li>
                        <li>
                            <strong>Issues</strong>: {meta.numIssues}
                        </li>
                        <li>
                            <strong>Unit Tests</strong>: {meta.numTests}
                        </li>
                        <li>
                            <strong><a className="link" href="https://gitlab.com/bhaver/relocccate/" target="_blank" rel="noopener noreferrer">Gitlab Repository</a></strong>
                        </li>
                    </ul>
                    <h2 className={"h2-title"}>API/Data Information</h2>
                    <ul>
                        <li>
                            <strong><a className="link" href="https://collegescorecard.ed.gov/data/documentation/" target="_blank" rel="noopener noreferrer">College Scorecard API</a></strong>
                        </li>
                        <li>
                            <strong><a className="link" href="https://developers.amadeus.com/self-service/category/destination-content/api-doc/safe-place/api-reference" target="_blank" rel="noopener noreferrer">Safe Place API</a></strong>
                        </li>
                        <li>
                            <strong><a className="link" href="https://openweathermap.org/api/one-call-api#history" target="_blank" rel="noopener noreferrer">OpenWeather API</a></strong>
                        </li>
                        <li>
                            <strong><a className="link" href="https://www.unitedstateszipcodes.org/zip-code-database/" target="_blank" rel="noopener noreferrer">Zip Code Database</a></strong>
                        </li>
                        <li>
                            <strong><a className="link" href="https://documenter.getpostman.com/view/14694719/Tz5jefMo" target="_blank" rel="noopener noreferrer">Postman Documentation</a></strong>
                        </li>
                    </ul>
                    <h2 className={"h2-title"}>Tools</h2>
                    <ul>
                        <li>
                            <strong><a className="link" href="https://www.postman.com/" target="_blank" rel="noopener noreferrer">Postman</a></strong>: Collaboration platform used for testing and designing APIs
                        </li>
                        <li>
                            <strong><a className="link" href="https://gitlab.com/" target="_blank" rel="noopener noreferrer">GitLab</a></strong>: Web-based Git repository
                        </li>
                        <li>
                            <strong><a className="link" href="https://reactjs.org/" target="_blank" rel="noopener noreferrer">React</a></strong>: JavaScript library used for frontend development
                        </li>
                        <li>
                            <strong><a className="link" href="https://www.docker.com/" target="_blank" rel="noopener noreferrer">Docker</a></strong>: Platform that simplifies the process of building, running. and managing applications
                        </li>
                        <li>
                            <strong><a className="link" href="https://aws.amazon.com/ec2/" target="_blank" rel="noopener noreferrer">AWS EC2</a></strong>: Platform used for hosting and deploying web applications
                        </li>
                        <li>
                            <strong><a className="link" href="https://nodejs.org/en/" target="_blank" rel="noopener noreferrer">NodeJS</a></strong>: Runtime environment that manages and executes JavaScript code
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        
    );
    
}

