import React from "react";
import "./AboutTop.css";
import InfoCards from "./InfoCards";
import {memberInfo} from "./MemberInfo";


export default function AboutTop() {
    return (
        <React.Fragment>
            <h1 className="about-title text-center">Welcome to Relocccate!</h1>
            <p className="description"> 
                Find your perfect neighborhood with the help of Relocccate. Whether you are
                moving to a new place finding out more about where you live,
                Relocccate will give you information on your your counties, cities, and nearby colleges.
                For safety data, navigate to the Cities tab. If you want more information about
                the local weather, head over to the Counties tab. To get statistics on nearby 
                colleges go to the Colleges tab. By combining different data about cities, colleges,
                and counties, you can get a holistic overview of where you live and compare nearby
                colleges and counties in the same city. 
            </p>
            <div className="row justify-content-center">
                <InfoCards info={memberInfo[0]}></InfoCards>
                <InfoCards info={memberInfo[1]}></InfoCards>
                <InfoCards info={memberInfo[2]}></InfoCards>
                <InfoCards info={memberInfo[3]}></InfoCards>
                <InfoCards info={memberInfo[4]}></InfoCards>
            </div>
        </React.Fragment>
    );
}