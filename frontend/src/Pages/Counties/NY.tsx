import * as React from "react";
import "./NY.css";

import { LatLngExpression } from "leaflet";
import "leaflet/dist/leaflet.css";
import { Link } from 'react-router-dom';
import Card from 'react-bootstrap/Card'
import CardGroup from 'react-bootstrap/CardGroup'
import { MapContainer, Popup, TileLayer, CircleMarker } from "react-leaflet";

import columbia from '../../images/Columbia.jpg'
import city from '../../images/NYCity.jpg'

function NY(){
    /* Define constants to be used for map component */
    /* NOTE: will refactor into interface for phase 2 to avoid repetition*/
    const position : LatLngExpression = [40.73, -73.99];
    const zoom = 15

    const color = { color: 'red', fillColor: 'red' }
    return (
        <div>
            <h2>New York County</h2>
            {/* Use MapContainer to create interactive map that allows the user to scroll and view geolocation */}
            <MapContainer center={position} zoom={zoom} scrollWheelZoom={false} style={{height : '300px'}}>
                <TileLayer
                    attribution="&copy; <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors"
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {/* create circle pinpoint at given longitude and latitude to identify core region */}
                <CircleMarker center={position} pathOptions={color} radius={40}>
                    <Popup>New York County Center</Popup>
                </CircleMarker>
            </MapContainer>
            {/* use CardGroup to display text in neat card format */}
            <CardGroup>
                <Card>
                    {/* display statistics in first card */}
                    <Card.Title> <strong> Weather Information </strong> </Card.Title>
                    <Card.Body> 
                        <Card.Text> Average Temperature (Celsius): 8.75 </Card.Text>
                        <Card.Text> Pressure: 1011 </Card.Text>
                        <Card.Text> Humidity: 34 </Card.Text>
                        <Card.Text> Wind Speed: 4.63 </Card.Text>
                        <Card.Text> UVI: 3.04 </Card.Text>
                    </Card.Body>
                </Card>
                <Card>
                    {/* display image and link to college in second card */}
                    <Card.Img variant="top" src={columbia} />
                    <Card.Body>
                        <Card.Title>Explore Nearby Universities</Card.Title>
                        <Card.Text>
                            <Link to="/Columbia">Columbia University</Link>
                        </Card.Text>
                    </Card.Body>
                </Card>
                <Card>
                    {/* display image and link to city in third card */}
                    <Card.Img variant="top" src={city} />
                    <Card.Body>
                        <Card.Title>Explore the City</Card.Title>
                        <Card.Text>
                            <Link to="/NYC">New York City, New York</Link>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </CardGroup>
        </div>
    )
}

export default NY;