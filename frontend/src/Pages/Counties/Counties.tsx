import React from "react";
import { Link } from 'react-router-dom';

import "./Counties.css";

import Table from 'react-bootstrap/Table'
function Counties(){
    return (
        <div style={{margin: 'auto', marginTop: '15px', width: '80%'}}> 
            <h2>Counties</h2>
            <Table striped bordered hover>
                {/* Create a table view to display all the elements for the counties */}
                <thead>
                    <tr>
                        <th>County</th>
                        <th>Latitude</th>
                        <th>Longitude</th>
                        <th>Average Temperature (Celsius)</th>
                        <th>Pressure</th>
                        <th>Humidity</th>
                        <th>Wind Speed</th>
                        <th>UVI</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><Link to="/NY">New York County</Link></td>
                        <td>40.73</td>
                        <td>-73.99</td>
                        <td>8.75</td>
                        <td>1011</td>
                        <td>34</td>
                        <td>4.63</td>
                        <td>3.04</td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td><Link to="/Travis">Travis County</Link></td>
                        <td>30.26</td>
                        <td>-97.74</td>
                        <td>12.79</td>
                        <td>1014</td>
                        <td>62</td>
                        <td>3.6</td>
                        <td>7.36</td>
                    </tr>
                </tbody>
                <tbody>
                    <tr>
                        <td><Link to="/DallasCounty">Dallas County</Link></td>
                        <td>32.98</td>
                        <td>-96.89</td>
                        <td>17.55</td>
                        <td>1021</td>
                        <td>31</td>
                        <td>4.12</td>
                        <td>5.62</td>
                    </tr>
                </tbody>
            </Table>
        </div>
    )
}

export default Counties;