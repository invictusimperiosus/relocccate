import React from "react";
import "./Travis.css";

import { LatLngExpression } from "leaflet";
import "leaflet/dist/leaflet.css";
import { Link } from 'react-router-dom';
import Card from 'react-bootstrap/Card'
import CardGroup from 'react-bootstrap/CardGroup'
import { MapContainer, Popup, TileLayer, CircleMarker } from "react-leaflet";

import UT from '../../images/UT.jpg'
import city from '../../images/AustinCity.jpg'

function Travis(){
    /* Define constants to be used for map component */
    /* NOTE: will refactor into interface for phase 2 to avoid repetition*/
    const position : LatLngExpression = [30.26, -97.74];
    const zoom = 15

    const color = { color: 'red', fillColor: 'red' }
    return (
        <div>
            <h2>Travis County</h2>
            {/* Use MapContainer to create interactive map that allows the user to scroll and view geolocation */}
            <MapContainer center={position} zoom={zoom} scrollWheelZoom={false} style={{height : '300px'}}>
                <TileLayer
                    attribution="&copy; <a href='http://osm.org/copyright'>OpenStreetMap</a> contributors"
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {/* create circle pinpoint at given longitude and latitude to identify core region */}
                <CircleMarker center={position} pathOptions={color} radius={40}>
                    <Popup>Travis County Center</Popup>
                </CircleMarker>
            </MapContainer>
            {/* use CardGroup to display text in neat card format */}
            <CardGroup>
                <Card>
                    {/* display statistics in first card */}
                    <Card.Title> <strong> Weather Information </strong> </Card.Title>
                    <Card.Body> 
                        <Card.Text> Average Temperature (Celsius): 12.79 </Card.Text>
                        <Card.Text> Pressure: 1014 </Card.Text>
                        <Card.Text> Humidity: 62 </Card.Text>
                        <Card.Text> Wind Speed: 3.6 </Card.Text>
                        <Card.Text> UVI: 7.36 </Card.Text>
                    </Card.Body>
                </Card>
                <Card>
                    {/* display image and link to college in second card */}
                    <Card.Img variant="top" src={UT} />
                    <Card.Body>
                        <Card.Title>Explore Nearby Universities</Card.Title>
                        <Card.Text>
                            <Link to="/Uta">UT Austin</Link>
                        </Card.Text>
                    </Card.Body>
                </Card>
                <Card>
                    {/* display image and link to city in third card */}
                    <Card.Img variant="top" src={city} />
                    <Card.Body>
                        <Card.Title>Explore the City</Card.Title>
                        <Card.Text>
                            <Link to="/Austin">Austin, Texas</Link>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </CardGroup>
        </div>
    )
}

export default Travis;