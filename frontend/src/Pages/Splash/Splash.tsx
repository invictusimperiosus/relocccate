import React from "react";
import "./Splash.css";
import { BsCaretDown } from "react-icons/bs";
import Models from "./Models";

function Splash(){
    function click(){
        window.scrollTo({top:650, left: 0, behavior:'smooth'});
    }
    return (
        <div className="Landing">
            <header className="Landing-header">
                <div style={{marginTop:"150px"}}>
                    <h3> Relocccate</h3>
                    <h1>To anywhere you want</h1>
                </div>
                <div className="arrow">
                    <BsCaretDown size={75} onClick={()=>click()}/>
                </div>
            </header>
            <div className="body">
                <div className="purpose">
                    <div className="purpose-header">
                        Make your move easier
                        <Models />
                    </div>
                </div>
            </div>
        </div>
      );
    
}

export default Splash;