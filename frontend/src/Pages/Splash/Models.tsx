// import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';
// import ButtonBase from '@material-ui/core/ButtonBase';
// import Typography from '@material-ui/core/Typography';
import { NavLink } from 'react-router-dom';
// import Image from "react-bootstrap/Image";

import weather from "./weather3.jpg";
import college from "./college.jpg";
import "./Models.css";
import safety from "./safety2.jpeg";

/* used to display model page buttons
on landing page */
function Models() {
  /* display the three model buttons */
  return (
    <div className="row pt-5 justify-content-md-center mx-0">
        <div className="col-lg-5 ">
          <NavLink to="/colleges">
            <img
              src={college}
              className="rounded-circle mx-auto d-block mb-5"
              alt="college"
              style={{ width: "30rem" }}
            ></img>
          </NavLink>
        </div>
        <div className="col-lg-5" style={{ marginTop: "5rem" }}>
          <h1 className="college-text">
            Get the latest stats on nearby colleges
          </h1>
        </div>

        <div className="row pt-5 justify-content-md-center">
        <div
          className="col-lg-5 order-last order-lg-first"
          style={{ marginTop: "5rem" }}
        >
          <h1 className="font-weight-bold text-center p-4">
            
          </h1>
          <h5 className="counties-text">
            Find the county with the perfect weather
          </h5>
        </div>

        <div className="col-lg-4">
          <NavLink to="/counties">
            <img
              src={weather}
              className="rounded-circle mx-auto d-block mb-5 "
              alt="counties"
              style={{ width: "30rem" , height: "30rem"}}
            ></img>
          </NavLink>
        </div>
      </div>

      <div className="row pt-5 justify-content-md-center mx-0">
        <div className="col-lg-5 ">
          <NavLink to="/cities">
            <img
              src={safety}
              className="rounded-circle mx-auto d-block mb-5"
              alt="safety"
              style={{ width: "30rem", height: "30rem" }}
            ></img>
          </NavLink>
        </div>
        <div className="col-lg-5" style={{ marginTop: "5rem" }}>
          <h1 className="cities-text">
            Stay safe in your city 
          </h1>  
        </div>
      </div>
    </div>
  );
}

export default Models;