import React from "react";
import ReactPlayer from "react-player"
import "./utd.css";
import { Link } from 'react-router-dom';

function Utd(){
    return (
        <div className = "utd-background">
            <h1 className = "utd-title">The University of Texas at Dallas</h1>
            <div className = "utd-college-pic"></div> {/* Picture of UTD */}
            <div className = "text-center">
                <div className = "utd-text">
                    <p>
                        State: TX
                    </p>
                    <p>
                        Latitude: 32.984673
                    </p>
                    <p>
                        Longitude: -96.749702
                    </p>
                    <p>
                        Admission Rate: 0.6922
                    </p>
                    <p>
                        Average SAT Score: 1330
                    </p>
                    <p>
                        Student Population Size: 30572
                    </p>
                </div>
            </div>
            <div className="dallas-link">
                City: <Link to="/Dallas">Richardson</Link> {/* Link to Dallas city page */}
            </div>
            <div className="dallas-pic"></div>
            <div className="dallas-county-link">
                County: <Link to="/DallasCounty">Dallas County</Link> {/* Link to Dallas county page */}
            </div>
            <div className="dallas-county-pic"></div>
            <div className="uta-vid"> {/* Video about UTD */}
                <ReactPlayer
                    url="https://www.youtube.com/watch?v=WwC4nTsrRD8"
                />
            </div>
        </div>
    )
}


export default Utd;