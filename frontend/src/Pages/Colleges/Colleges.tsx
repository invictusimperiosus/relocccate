
import "./Colleges.css";
import { Container, Row, Col } from 'react-grid-system';
import { Link } from 'react-router-dom';

function Colleges(){
    return (
        <div style={{margin: 'auto', marginTop: '15px', width: '80%'}}>
            <h2>Colleges</h2>
            <Container>  {/* Create grid */}
                <Row>
                    <Col>
                        <div className = "college">
                            <div className = "uta-pic"></div>
                            <div className = "text">
                                <p>
                                    <Link to="/Uta">The University of Texas at Austin</Link>  {/* Link to UTA page */}
                                </p>
                                <p>
                                    City: Austin
                                </p>
                                <p>
                                    State: TX
                                </p>
                                <p>
                                    Latitude: 30.282825
                                </p>
                                <p>
                                    Longitude: -97.738273
                                </p>
                                <p>
                                    Admission Rate:  0.3852
                                </p>
                                <p>
                                    Average SAT Score: 1367
                                </p>
                            </div>
                        </div>
                    </Col>
                    <Col>
                        <div className = "college">
                            <div className = "utd-pic"></div>
                            <div className = "text">
                                <p>
                                    <Link to="/Utd">The University of Texas at Dallas</Link> {/* Link to UTD page */}
                                </p>
                                <p>
                                    City: Richardson
                                </p>
                                <p>
                                    State: TX
                                </p>
                                <p>
                                    Latitude: 32.984673
                                </p>
                                <p>
                                    Longitude: -96.749702
                                </p>
                                <p>
                                   Admission Rate: 0.6922
                                </p>
                                <p>
                                    Average SAT Score: 1330
                                </p>
                            </div>
                        </div>
                    </Col>
                    <Col>
                        <div className = "college">
                            <div className = "columbia-pic"></div>
                            <div className = "text">
                                <p>
                                    <Link to="/Columbia">Columbia University in the City of New York</Link> {/* Link to Columbia page */}
                                </p>
                                <p>
                                    City: New York
                                </p>
                                <p>
                                    State: NY
                                </p>
                                <p>
                                    Latitude: 40.808286
                                </p>
                                <p>
                                    Longitude: -73.961885
                                </p>
                                <p>
                                    Admission Rate: 0.0591
                                </p>
                                <p>
                                    Average SAT Score: 1512
                                </p>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}


export default Colleges;