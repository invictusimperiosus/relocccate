import React from 'react'
import ReactPlayer from "react-player"
import "./uta.css";
import { Link } from 'react-router-dom';

function Uta(){
    return (
        <div className = "uta-background">
            <h1 className = "uta-title">The University of Texas at Austin</h1>
            <div className = "uta-college-pic"></div> {/* Picture of UTA */}
            <div className = "text-center">
                <div className = "uta-text">
                    <p>
                        State: TX
                    </p>
                    <p>
                        Latitude: 30.282825
                    </p>
                    <p>
                        Longitude: -97.738273
                    </p>
                    <p>
                        Admission Rate: 0.3852
                    </p>
                    <p>
                        Average SAT Score: 1367
                    </p>
                    <p>
                        Student Population Size: 55097
                    </p>
                </div>
            </div>
            <div className="austin-link">
                City: <Link to="/Austin">Austin</Link> {/* Link to Austin city page */}
            </div>
            <div className="austin-pic"></div>
            <div className="travis-link">
                County: <Link to="/Travis">Travis County</Link> {/* Link to Travis county page */}
            </div>
            <div className="travis-pic"></div>
            <div className="uta-vid"> {/* Video about UTA */}
                <ReactPlayer
                    url="https://www.youtube.com/watch?v=N9UmB9re0ls"
                />
            </div>
        </div>
    )
}


export default Uta;