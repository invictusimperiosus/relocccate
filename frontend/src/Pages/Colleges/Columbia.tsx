import React from 'react'
import ReactPlayer from "react-player"
import "./columbia.css";
import { Link } from 'react-router-dom';

function Columbia(){
    return (
        <div className = "columbia-background">
            <h1 className = "columbia-title">Columbia University in the City of New York</h1>
            <div className = "columbia-college-pic"></div> {/* Picture of Columbia University */}
            <div className = "text-center">
                <div className = "columbia-text">
                    <p>
                        State: NY
                    </p>
                    <p>
                        Latitude: 40.808286
                    </p>
                    <p>
                        Longitude: -73.961885
                    </p>
                    <p>
                        Admission Rate: 0.0591
                    </p>
                    <p>
                        Average SAT Score: 1512
                    </p>
                    <p>
                        Student Population Size: 32789
                    </p>
                </div>
            </div>
            <div className="newyork-link">
                City: <Link to="/NYC">New York</Link> {/* Link to New York city page */}
            </div>
            <div className="newyork-pic"></div>
            <div className="newyork-county-link">
                County: <Link to="/NY">New York County</Link> {/* Link to New York county page */}
            </div>
            <div className="newyork-county-pic"></div>
            <div className="uta-vid">
                <ReactPlayer
                    url="https://www.youtube.com/watch?v=7cwUcdpUayQ"
                />
            </div>
        </div>
    )
}


export default Columbia;