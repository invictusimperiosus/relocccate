import React from "react";
import "bootstrap";
import {BrowserRouter as Router, Switch, Route } from "react-router-dom";


import Cities from "./Pages/Cities/Cities";
import Colleges from "./Pages/Colleges/Colleges";
import Counties from "./Pages/Counties/Counties";
import Splash from "./Pages/Splash/Splash";
import About from "./Pages/About/About";
import Uta from "./Pages/Colleges/Uta";
import Utd from "./Pages/Colleges/Utd";
import Columbia from "./Pages/Colleges/Columbia";

import NYC from "./Pages/Cities/NYC";
import Austin from "./Pages/Cities/Austin";
import Dallas from "./Pages/Cities/Dallas";

import NY from "./Pages/Counties/NY";
import Travis from "./Pages/Counties/Travis";
import DallasCounty from "./Pages/Counties/Dallas";

import Navbar from "./Components/Navbar";


function App(){
  return (
    <Router> 
      <div>
        <Navbar />
        <Switch>
          <Route exact path='/'> <Splash /></Route>
          <Route path='/cities'> <Cities /></Route>
          <Route path='/colleges'> <Colleges /></Route>
          <Route path='/counties'> <Counties /></Route>
          <Route path='/About'> <About /></Route>
          <Route path='/Uta'> <Uta /></Route>
          <Route path='/Utd'> <Utd /></Route>
          <Route path='/Columbia'> <Columbia /></Route>

          <Route path='/NYC'> <NYC /></Route>
          <Route path='/Austin'> <Austin /></Route>
          <Route path='/Dallas'> <Dallas /></Route>

          <Route path='/NY'> <NY /></Route>
          <Route path='/Travis'> <Travis /></Route>
          <Route path='/DallasCounty'> <DallasCounty /></Route>
          
          <Route path='/about'> <About /></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;